package com.notesonjava.jodd.json;

import java.time.ZonedDateTime;

import jodd.typeconverter.TypeConverter;
import jodd.typeconverter.TypeConverterManager;

public class TypeRegister {

	public static void registerExtensions(){
		TypeConverterManager manager = TypeConverterManager.get();
		
		manager.register(ZonedDateTime.class,  new TypeConverter<ZonedDateTime>() {
			@Override
			public ZonedDateTime convert(Object value) {
				String stringValue = value.toString().trim();
				return ZonedDateTime.parse(stringValue);
			}
		});

	}

}

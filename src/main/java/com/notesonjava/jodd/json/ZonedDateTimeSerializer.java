package com.notesonjava.jodd.json;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import jodd.json.JsonContext;
import jodd.json.TypeJsonSerializer;

public class ZonedDateTimeSerializer implements TypeJsonSerializer<ZonedDateTime> {
	@Override
	public boolean serialize(JsonContext jsonContext, ZonedDateTime value) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;
		String result = value.format(formatter);
		jsonContext.writeString(result);
		return true;
	}
}